[中性回测] [年化收益/最大回撤 18.64] 基于 GBDT多因子回归框架 + HHHRegrFactor(自研)因子 + 涨跌幅max + Slippage(自研)过滤构建中性策略

## 一、因子构建思路

基于GBDT多因子回归框架构建因子：
[https://bbs.quantclass.cn/thread/20404](https://bbs.quantclass.cn/thread/20404)

构建思路与框架中的样例因子基本相同，具体分析见：
[https://bbs.quantclass.cn/thread/21492)(https://bbs.quantclass.cn/thread/21492)

在样例因子的基础上，做了如下调整。

#### 1、隐式hold hour
在[21492](https://bbs.quantclass.cn/thread/21492)文中分析过，在船队框架中使用较长的hold hour存在过滤器无法有效发挥作用的风险。因此我们希望在把hold hour的逻辑隐藏在因子中，以在船队框架中直接设定hold_hour=1。
由于我们在训练时已经对回归目标做过归一化，这里直接对回归后的因子取hold_hour为窗口的平均即可达到近似hold hour的效果。

```
    df_all[col_name] = df_all.groupby('symbol', group_keys=False)[col_name].apply(
            lambda x: x.rolling(self.suggest_hold_hour, min_periods=1).mean()
        )
```
（这里写的是df_all，但对于单币的df是通用的）

也是因为这点，我把因子命名为Hidden Hold Hour Regression Factor(HHHRegrFactor)。

#### 2、增加元因子Boll Count与Boll Count V1
Boll Count参考船队因子库中的Boll_count.py。

Boll Count V1见aaZXN老板的文章[https://bbs.quantclass.cn/thread/21136](https://bbs.quantclass.cn/thread/21136)。
我在加Boll Count的时候看到群里讨论就顺手加了进来。

#### 3、增加Slippage过滤因子
思路来源于邢大2023年分享会直播01期的流动性溢价选股策略，船队因子库中也有类似的Liquidity_v3。

Slippage因子的设计目标为估算单位资金可能造成的滑点。具体代码见附件及下文。

在策略中，过滤器参数设置的很宽松，仅过滤少数极端情况。不过在模型训练时会“借用”此因子对训练样本的权重做惩罚。

#### 4、训练样本权重调整
在原版样例中，全部样本默认采用等权处理。在这个版本中，我们弃用了Volume过滤因子，但对应了原版rank<=60这个参数，为头部+尾部共60个币种略微增加了训练权重，

在计算头部、尾部币种排名时，使用了Slippage因子估算滑点对预期收益率做了惩罚。


#### 5、LightGBM训练参数调整
沿用[21492](https://bbs.quantclass.cn/thread/21492)文中讨论的思路，增加了一些超参数从不同的角度综合限制模型的过拟合，提高泛化能力的同时也不要让模型的拟合能力损失太多。

```
hyper_paras = {
    'feature_fraction': 0.7, 'learning_rate': 0.1, 'lambda_l1': 0.01, 'lambda_l2': 0.1, 'max_depth': 10, 'num_leaves': 512, 'min_data_in_leaf': 5760
}
```


## 二、寻参

机器学习模型涉及到的超参数比较多，这里以主观设定为主，部分参数使用optuna辅助遍历。

## 三、选币因子代码


#### HHHRegrFactor选币因子
\src_backtesting\factors\HHHRegrFactor.py

详见附件，以下为元因子部分的计算

```
            df_feat[f'ROC{d}'] = df_single['close'].shift(d) / df_single['close']
            df_feat[f'MA{d}'] = df_single['close'].rolling(d, min_periods=1).mean() / df_single['close']
            df_feat[f'STD{d}'] = df_single['close'].rolling(d, min_periods=1).std() / df_single['close']
            df_feat[f'VWAP{d}'] = df_single['quote_volume'].rolling(d, min_periods=1).sum() / df_single[
                'volume'].rolling(
                d, min_periods=1).sum() / df_single['close']
            df_feat[f'BETA{d}'] = beta_func(df_single['close'], d) / df_single['close']
            df_feat[f'RESI{d}'] = 1 - lineagg_func(df_single['close'], d) / df_single['close']
            df_feat[f'QTLU{d}'] = df_single['close'].rolling(d, min_periods=1).quantile(0.8) / df_single['close']
            df_feat[f'QTLD{d}'] = df_single['close'].rolling(d, min_periods=1).quantile(0.2) / df_single['close']
            df_feat[f'RANK{d}'] = df_single['close'].rolling(d, min_periods=1).rank(pct=True)
            df_feat[f'IMAX{d}'] = df_single['high'].rolling(d, min_periods=1).apply(
                lambda x: x.argmax() + 1, raw=True) / d
            df_feat[f'IMIN{d}'] = df_single['low'].rolling(d, min_periods=1).apply(
                lambda x: x.argmin() + 1, raw=True) / d
            df_feat[f'IMXD{d}'] = df_feat[f'IMAX{d}'] - df_feat[f'IMIN{d}']
            df_feat[f'CORR{d}'] = df_single['close'].rolling(d, min_periods=1).corr(np.log(df_single['volume'] + 1))
            df_feat[f'CORD{d}'] = (df_single['close'] / df_single['close'].shift(1)).rolling(d, min_periods=1).corr(
                np.log(df_single['volume'] / df_single['volume'].shift(1) + 1))
            wvma_tmp = (df_single['close'] / df_single['close'].shift(1) - 1).abs() * df_single['volume']
            df_feat[f'WVMA{d}'] = wvma_tmp.rolling(d, min_periods=1).std() / (
                    wvma_tmp.rolling(d, min_periods=1).mean() + 1e-12)

            mean_tmp = df_single['close'].rolling(d).mean()
            std_tmp = df_single['close'].rolling(d).std(ddof=0)
            upper_tmp = mean_tmp + 2 * std_tmp
            lower_tmp = mean_tmp - 2 * std_tmp
            upper_count_tmp = (df_single['close'] > upper_tmp).rolling(d, min_periods=1).sum()
            lower_count_tmp = (df_single['close'] < lower_tmp).rolling(d, min_periods=1).sum()
            df_feat[f'BOLL_COUNT{d}'] = (upper_count_tmp - lower_count_tmp) / d
            df_feat[f'BOLL_COUNT_UP{d}'] = upper_count_tmp / d
            df_feat[f'BOLL_COUNT_DN{d}'] = lower_count_tmp / d

            diff_c_tmp = df_single['close'] / df_single['close'].shift(d)
            upper_v1_tmp = mean_tmp + std_tmp * (diff_c_tmp + diff_c_tmp ** (-1))
            lower_v1_tmp = mean_tmp - std_tmp * (diff_c_tmp + diff_c_tmp ** (-1))
            upper_count_v1_tmp = (df_single['close'] > upper_v1_tmp).rolling(d, min_periods=1).sum()
            lower_count_v1_tmp = (df_single['close'] < lower_v1_tmp).rolling(d, min_periods=1).sum()
            df_feat[f'BOLLV1_COUNT{d}'] = (upper_count_v1_tmp - lower_count_v1_tmp) / d
            df_feat[f'BOLLV1_COUNT_UP{d}'] = upper_count_v1_tmp / d
            df_feat[f'BOLLV1_COUNT_DN{d}'] = lower_count_v1_tmp / d
```


#### **涨跌幅max过滤因子**
\src_backtesting\filters\涨跌幅max.py

```
def signal(*args):
	df = args[0]
	n  = args[1]
	factor_name = args[2]
	df['该小时涨跌幅']=abs(df['close'].pct_change(1))
	df[factor_name] =df['该小时涨跌幅'].rolling(n).max()
	return df
```

#### **Slippage过滤因子**
\src_backtesting\filters\Slippage.py

```
def signal(*args):
    df = args[0]
    n = args[1]
    factor_name = args[2]
    price_path = 2 * (df['high'] - df['low']) - abs(df['open'] - df['close'])
    price_path /= df['close']
    chg_ratio = price_path / df['quote_volume']
    df[factor_name] = chg_ratio.rolling(n, min_periods=1).mean()
    return df
```

## 四、框架代码使用前配置须知

首先需要注意引入[20404](https://bbs.quantclass.cn/thread/20404)框架中的相关代码及相关依赖。

然后可以下载附件中的模型文件跳过训练过程，直接进入船队通用的数据整理+回放流程。

f1_中性策略回放.py 脚本，

```
compound_name = 'HHHRegrFactor'  # name
# ===常规配置
cal_factor_type = 'cross'  # cross/ vertical
start_date = '2022-01-01'
end_date = '2023-01-26'

factor_long_list = [('HHHRegrFactor', True, 1920, 0, 0.5)]
factor_short_list = [('HHHRegrFactor', True, 1920, 0, 0.5)]

trade_type = 'swap'
playCfg['c_rate'] = 6 / 10000  # 手续费
playCfg['hold_hour_num'] = 1  # hold_hour
# 只跑指定的N个offset,空列表则全offset
long_select_offset = []
short_select_offset = []

# ===回放增强配置
playCfg['long_coin_num'] = 10  # 多头选币数
playCfg['short_coin_num'] = 10  # 空头选币数
# 多币权重参数(不小于0),多币时有效,详见https://bbs.quantclass.cn/thread/8835
playCfg['long_p'] = 0  # 0 :等权, 0 -> ∞ :多币头部集中度逐渐降低
playCfg['short_p'] = 0  # long_coin_num = 3, long_p = 1 ;rank 1,2,3 的资金分配 [0.43620858, 0.34568712, 0.21810429]
playCfg['leverage'] = 1  # 杠杆率
playCfg['long_risk_position'] = 0  # 多头风险暴露 0.1 对冲后 10% 净多头, -0.2 对冲后 20% 净空头
playCfg['initial_trade_usdt'] = 10000  # 初始投入,金额过小会导致某些币无法开仓
# offset 止盈止损,都为0时,该功能关闭
playCfg['offset_stop_win'] = 0  # offset 止盈
playCfg['offset_stop_loss'] = 0  # offset 止损

# 固定白名单 'BTCUSDT'
long_white_list = []
short_white_list = []

# 固定黑名单
long_black_list = []
short_black_list = []
# ===过滤配置(元素皆为字符串,仿照e.g.写即可 支持 & |)
# 前置过滤 筛选出选币的币池

filter_before_params = [
    ['df1', '涨跌幅max_fl_120', 'value', 'lte', 0.2, RankAscending.FALSE, FilterAfter.FALSE],
    ['df2', '涨跌幅max_fl_120', 'value', 'lte', 0.2, RankAscending.FALSE, FilterAfter.FALSE],
    ['df1', 'Slippage_fl_120', 'value', 'lte', 0.2 / 1e5, RankAscending.FALSE, FilterAfter.FALSE],
    ['df2', 'Slippage_fl_120', 'value', 'lte', 0.2 / 1e5, RankAscending.FALSE, FilterAfter.FALSE],
]
filter_before_exec = [filter_generate(param=param) for param in filter_before_params]
```

## 五、所有币种因子组合样本内回测结果

**回测binance永续合约2022年1月1日 ～ 2023年01月26日**

```
start_date = '2022-01-01'
end_date   = '2023-01-26'
```

#### **2022年1月1日 ～ 2023年01月26日永续合约数据回测结果：**

平均"年化收益/回撤比" 18.64

初始投入资产: 10000.0 U,最终账户资产: 29402.11 U, 共支付手续费: 3541.05 U

|               |   累积净值 |   手续费磨损净值 |   年化收益 |   月化收益 |   月信息比 |   月化波动 |   月换手率 |   月化收益回撤比 | 最大回撤   | 最大回撤开始时间    | 最大回撤结束时间    |   盈利周期数 |   亏损周期数 | 胜率   | 每周期平均收益   |   盈亏收益比 | 单周期最大盈利   | 单周期大亏损   |   最大连续盈利周期数 |   最大连续亏损周期数 |   交易费率 |   leverage |
|:--------------|-----------:|-----------------:|-----------:|-----------:|-----------:|-----------:|-----------:|-----------------:|:-----------|:--------------------|:--------------------|-------------:|-------------:|:-------|:-----------------|-------------:|:-----------------|:---------------|---------------------:|---------------------:|-----------:|-----------:|
| HHHRegrFactor |       2.94 |         0.579221 |      1.744 |      0.088 |      1.429 |      0.061 |    11.7649 |         0.941176 | -9.35%     | 2022-12-28 19:00:00 | 2023-01-20 09:00:00 |         4854 |         4508 | 51.85% | 0.012%           |         1.08 | 2.94%            | -2.85%         |                   11 |                   10 |          6 |          1 |


#### 策略资金曲线

![hhh_playback](hhh_playback.png)

#### 各offset回测数据：

|    |   累积净值 | 最大回撤   | 最大回撤开始时间    | 最大回撤结束时间    |   盈利周期数 |   亏损周期数 | 胜率   | 每周期平均收益   |   盈亏收益比 | 单周期最大盈利   | 单周期大亏损   |   最大连续盈利周期数 |   最大连续亏损周期数 | 年化收益   |   年化收益/回撤比 |
|---:|-----------:|:-----------|:--------------------|:--------------------|-------------:|-------------:|:-------|:-----------------|-------------:|:-----------------|:---------------|---------------------:|---------------------:|:-----------|------------------:|
|  0 |       2.94 | -9.35%     | 2022-12-28 18:00:00 | 2023-01-20 08:00:00 |         4854 |         4508 | 51.85% | 0.01%            |         1.08 | 2.94%            | -2.85%         |                   11 |                   10 | 1.74 倍    |             18.64 |

#### 各offset资金曲线：

![hhh_offset](hhh_offset.png)

# 六、总结

虽然回测效果相比原版样例提升了不少，但很多细节处理的比较粗糙，依然是比较初步的探索，实盘有效性也仍待验证。希望有更多老板加入共同研究。
