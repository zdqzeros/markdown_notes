# TensorRT
Homepage [https://developer.nvidia.com/tensorrt]

### PyTorch Example
[https://developer.nvidia.com/blog/speeding-up-deep-learning-inference-using-tensorrt-updated/]

Example流程:
1. PyTorch Model => ONNX
2. Import ONNX -> TensorRT
3. Config TensorRT Engine (batch size, precision)
4. ONNX => TRT
5. Deploy (TensorRT runtime API)

### Quick Start Guide
#### TensorRT Ecosystem
1. Conversion
    - TF-TRT
    - ONNX conversion
    - TensorRT API
3. Deployment
    - TensorFlow
    - TensorRT runtime API
    - NVIdIA Triton Inference Server

### Developer Guide
#### 1. 编程模型
**Builder**
Input:
- NetworkDefinition (ONNX/Layer Interface/Tensor Interface)
- BuilderConfig (How to optimize. E.g. precision, memory, cuda kernels)

Output:
- Engine

Function:
- Eliminate dead computation, fold constant, combines operations
- Redece precision

**Engine**
Input:
- ExecutionContext

Function:
- Inference (sync: execute / async: enqueue)

#### 2. Dynamic Shape
TensorRT默认根据input shape优化模型，如果input shape不确定，可用BuilderConfig - OptimizationProfile声明Shape的范围。

#### 3. DLA
Deep Learning Accelerator for NVIDIA SoCs.
将一部分计算从GPU挪到SoC(System-on-a-Chip)上。

#### 4. Polygraphy
Run and debug models in TensorRT and other frameworks (ONNX-Runtime)


# TVM
[https://tvm.apache.org/]

List item

![image.png](./image.png)
